/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1249568
 */
public abstract class Ponto2D extends Ponto {
    
    protected Ponto2D(){
        setX(0);
        setY(0);
        setZ(0);
    }
    
    protected Ponto2D(double x, double y, double z){
        setX(x);
        setY(y);
        setZ(z);
    }
    
}
